<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <!-- <link rel="icon" type="image/x-icon" href="assets/favicon.ico"> -->
        <title>Imersao 100K Sem Estoque a Nova Estrategia – V1 A – Gustavo Henrique</title>

        <!-- CSS -->
        <!-- <link rel="stylesheet" href="css/used.css"> -->
        <link rel="stylesheet" href="css/form.css">
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/responsividade.css">
        <link rel="stylesheet" href="css/fonts.css">
        <link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    </head>

    <body style="background-color: white !important">
        <?php require('content/section_1.php'); ?>
        <?php require('content/section_2.php'); ?>
    </body>

    <?php //require('default/footer.php'); ?>

</html>