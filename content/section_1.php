<div id="section1">
    <div class="mt-5"></div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-4 mt-5">
            <span class="largeText font-weight-semibold lh-42">
                Conheça o método que permitiu um <span class="text-bluefade">engenheiro frustrado</span>, com TDAH e depressão, se tornar um milionário em apenas 1 ano e 2 meses
            </span>
            <br><br>
            <span class="normalText font-weight-medium">
                Em 3 aulas, você vai descobrir a NOVA ESTRATÉGIA para criar uma loja virtual sem estoque no seu tempo livre e ter resultados 3x mais rápido pra vender mais de 100 mil reais todos os meses.
            </span>
            <div class="row">
                <div class="col-lg-9">
                    <a href="" class="btn btnParticipar mt-4 smallText font-weight-extrabold">QUERO PARTICIPAR</a>
                    <div class="mt-3 text-center">
                        INSCREVA-SE AGORA PELO BOTÃO ACIMA
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
    <br><br>
    <br><br>
</div>